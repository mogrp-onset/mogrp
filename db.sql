-- Adminer 4.2.5 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `inventories`;
CREATE TABLE `inventories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contents` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `players`;
CREATE TABLE `players` (
  `steam_id` varchar(17) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `phone_number` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `rights` int(11) NOT NULL DEFAULT '0' COMMENT '0 = player, 1000 = helper, 2000 = moderator, 3000 = admin, 4000 = superadmin',
  PRIMARY KEY (`steam_id`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `phone_number` (`phone_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `player_bank_transactions`;
CREATE TABLE `player_bank_transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `steam_id` varchar(17) COLLATE utf8_unicode_ci NOT NULL,
  `amount` int(11) NOT NULL,
  `from` varchar(17) COLLATE utf8_unicode_ci DEFAULT NULL,
  `to` varchar(17) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reason` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `steam_id` (`steam_id`),
  KEY `from` (`from`),
  KEY `to` (`to`),
  CONSTRAINT `player_bank_transactions_ibfk_1` FOREIGN KEY (`steam_id`) REFERENCES `players` (`steam_id`) ON DELETE CASCADE,
  CONSTRAINT `player_bank_transactions_ibfk_2` FOREIGN KEY (`from`) REFERENCES `players` (`steam_id`) ON DELETE SET NULL,
  CONSTRAINT `player_bank_transactions_ibfk_3` FOREIGN KEY (`to`) REFERENCES `players` (`steam_id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `player_properties`;
CREATE TABLE `player_properties` (
  `steam_id` varchar(17) COLLATE utf8_unicode_ci NOT NULL,
  `key` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`steam_id`,`key`),
  CONSTRAINT `player_properties_ibfk_1` FOREIGN KEY (`steam_id`) REFERENCES `players` (`steam_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `properties`;
CREATE TABLE `properties` (
  `key` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `vehicles`;
CREATE TABLE `vehicles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_steam_id` varchar(17) COLLATE utf8_unicode_ci DEFAULT NULL,
  `model_id` int(11) NOT NULL,
  `x` double NOT NULL,
  `y` double NOT NULL,
  `z` double NOT NULL,
  `h` double NOT NULL,
  `health` int(11) NOT NULL,
  `license_plate` varchar(13) COLLATE utf8_unicode_ci NOT NULL,
  `color_r` int(11) NOT NULL,
  `color_g` int(11) NOT NULL,
  `color_b` int(11) NOT NULL,
  `key_code` int(11) NOT NULL,
  `fuel` float NOT NULL,
  `inventory` text COLLATE utf8_unicode_ci NOT NULL,
  `inventory_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `owner_steam_id` (`owner_steam_id`),
  KEY `inventory_id` (`inventory_id`),
  CONSTRAINT `vehicles_ibfk_1` FOREIGN KEY (`owner_steam_id`) REFERENCES `players` (`steam_id`) ON DELETE SET NULL,
  CONSTRAINT `vehicles_ibfk_2` FOREIGN KEY (`inventory_id`) REFERENCES `inventories` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- 2020-08-04 09:30:30
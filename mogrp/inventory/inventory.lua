Inventory = {}
Inventory.__index = Inventory

-- TODO Equip/Unequip item

ITEMS_CONFIG = json_load("items.json")
local ITEMS_FNCS = {}
local TYPES_FNCS = {}
local CACHE = Cache:new(3600)
local handle

AddEvent("OnPackageStart", function()
    handle = GetDbHandle("general")
end)

function GetInventory(id, fnc)
    print("Retrieving inventory data...")
    local inventory = CACHE:get(id)
    if not inventory then
        print("Inventory not in cache, retrieving data from DB")
        mariadb_async_query(handle, mariadb_prepare(handle,
                "SELECT contents FROM inventories WHERE id = ?",
                id
        ), function()
            print("Data retrieved")
            local inventory
            if mariadb_get_row_count() ~= 0 then
                print("Inventory found !")
                local result = mariadb_get_assoc(1)
                print(result)
                inventory = Inventory:new(json_decode(result['contents']))
                inventory.id = id
                CACHE:put(id, inventory)
            end
            fnc(inventory)
        end)
    else
        print("Inventory in cache")
        print(inventory)
        fnc(inventory)
    end
end

local function SaveInventory(inventory, fnc)
    if not inventory.id then
        mariadb_async_query(handle, mariadb_prepare(handle,
                "INSERT INTO inventories (contents) VALUES ('?')",
                inventory:serialize()
        ), function()
            inventory.id = mariadb_get_insert_id()
            CACHE:put(inventory.id, inventory)
            if fnc then
                fnc(inventory)
            end
        end)
    else
        mariadb_async_query(handle, mariadb_prepare(handle,
                "REPLACE INTO inventories (id, contents) VALUES (?, '?')",
                inventory.id,
                inventory:serialize()
        ), function()
            CACHE:put(inventory.id, inventory)
            if fnc then
                fnc(inventory)
            end
        end)
    end
end

function Inventory:create(data, fnc)
    SaveInventory(Inventory:new(data), function(inventory)
        fnc(inventory)
    end)
end

function Inventory:new(inventory)
    if not inventory then
        inventory = {}
    end
    setmetatable(inventory, Inventory)
    if not inventory.items then
        inventory.items = {}
    end
    if not inventory.maxWeight then
        inventory.maxWeight = 0
    end
    if not inventory.weight then
        inventory.weight = 0
    end
    inventory.observers = {}
    return inventory
end

function Inventory:addObserver(player)
    self.observer[player] = true
end

function Inventory:removeObserver(player)
    self.observer[player] = nil
end

function Inventory:sendItemUpdate(item, quantity, equipped)
    for observer in pairs(self.observers) do
        SendInventoryUpdate(observer, self.id, item, quantity, equipped)
    end
end

function Inventory:addItem(item, quantity)
    if ITEMS_CONFIG[item] then
        local weight = ITEMS_CONFIG[item].weight * quantity
        if self.weight + weight > self.maxWeight then
            return false
        end
        local itemData = self.items[item]
        if itemData then
            self.items[item].quantity = self.items[item].quantity + quantity
        else
            self.items[item] = {
                quantity = quantity
            }
        end
        self:sendItemUpdate(item, self.items[item].quantity, self.items[item].equipped)
        SaveInventory(self)
        return true
    end
    return false
end

function Inventory:clearItem(item)
    if ITEMS_CONFIG[item] and self.items[item] then
        self.weight = self.weight - (self.items[item].quantity * ITEMS_CONFIG[item].weight)
    end
    self:sendItemUpdate(item, 0)
    self.items[item] = nil
    SaveInventory(self)
end

function Inventory:removeItem(item, quantity)
    if quantity < 0 or not self.items[item] or quantity > self.items[item].quantity then
        return false
    end
    self.weight = self.weight - (quantity * ITEMS_CONFIG[item].weight)
    self.items[item].quantity = self.items[quantity] - quantity
    if self.items[item].quantity <= 0 then
        self:clearItem(item)
    else
        SaveInventory(self)
    end
    self:sendItemUpdate(item, self.items[item].quantity, self.items[item].equipped)
    return true
end

function Inventory:getItem(item)
    return self.items[item]
end

function Inventory:useItem(player, item, quantity)
    local itemCfg = ITEMS_CONFIG[item]
    if not itemCfg then
        self:clearItem(item)
        return false
    end
    if quantity < 0 or not self.items[item] or quantity > self.items[item].quantity then
        return false
    end
    local typeFnc = TYPES_FNCS[itemCfg.type]
    local fnc = ITEMS_FNCS[item]
    local consumed = false
    if fnc then
        consumed = fnc(player, item, itemCfg, quantity)
    elseif typeFnc then
        consumed = typeFnc(player, item, itemCfg, quantity)
    end
    if consumed then
        self:removeItem(item, quantity)
    end
    return true
end

function Inventory:transferItem(otherInventoryId, item, quantity, fct)
    GetInventory(otherInventoryId, function(otherInventory)
        if otherInventory then
            if self.items[item] and self.items[item].quantity >= quantity
                    and otherInventory.weight + (ITEMS_CONFIG[item].weight * quantity) <= otherInventory.maxWeight then
                if self:removeItem(item, quantity) then
                    otherInventory:addItem(item, quantity)
                    fct(true)
                else
                    fct(false)
                end
            else
                fct(false)
            end
        else
            fct(false)
        end
    end)
end

function Inventory:getItems()
    return self.items
end

function Inventory:serialize()
    return json_encode(self)
end

function Inventory.RegisterTypeFunction(type, fnc)
    TYPES_FNCS[type] = fnc
end

function Inventory.RegisterItemFunction(item, fnc)
    ITEMS_FNCS[item] = fnc
end

function Inventory.RegisterItemConfig(item, config)
    ITEMS_CONFIG[item] = config
end

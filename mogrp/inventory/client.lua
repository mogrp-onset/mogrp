local SHOW_CATEGORIES = {}
SHOW_CATEGORIES= {}
for i,category in pairs(INVENTORY_CONFIG.categories) do
    SHOW_CATEGORIES[i] = category.id
end

local inventoryUI
inventoryIsOpen = false

function OnPackageStartInventory()
    inventoryUI = CreateWebUI(0, 0, 0, 0, 10, 60)
    LoadWebFile(inventoryUI, "http://asset/"..GetPackageName().."/burdigalax/client/gui/inventory.html")
    SetWebAlignment(inventoryUI, 0.0, 0.0)
    SetWebAnchors(inventoryUI, 0.0, 0.0, 1.0, 1.0)
    SetWebVisibility(inventoryUI, WEB_HIDDEN)
end
AddEvent("OnPackageStart", OnPackageStartInventory)

function OnPackageStopInventory()
    DestroyWebUI(inventoryUI)
end
AddEvent("OnPackageStop", OnPackageStopInventory)

local function CreateInventoryConfig(id, title, maxWeight)
    local inventoryConfig = {
        id = id,
        storageSize = maxWeight,
        name = title,
        description = title,
        selectName = title,
        --              selectedNearbyInventoryId: 26,
        --              nearbyInventoriesIds:[26, 27, 33],
        categoriesIds = SHOW_CATEGORIES,
        items = {}
    }
    return inventoryConfig
end

AddRemoteEvent("inventory:open", function(id, title, maxWeight, secondId, secondTitle, secondMaxWeight)
    AddPlayerChat("InventoryOpen("..id..", "..title..", "..maxWeight..")")
    inventoryIsOpen = true

    INVENTORY_CONFIG.mainInventoryId = id
    INVENTORY_CONFIG.inventories = {}

    INVENTORY_CONFIG.inventories[1] = CreateInventoryConfig(id, title, maxWeight)

    if secondInventory then
        INVENTORY_CONFIG.inventories[2] = CreateInventoryConfig(secondId, secondTitle, secondMaxWeight)
        INVENTORY_CONFIG.inventories[1].selectedNearbyInventoryId = INVENTORY_CONFIG.inventories[2].id
        INVENTORY_CONFIG.inventories[1].nearbyInventoriesIds = {INVENTORY_CONFIG.inventories[2].id}
        INVENTORY_CONFIG.inventories[2].nearbyInventoriesIds = {INVENTORY_CONFIG.inventories[1].id}
    end

    ExecuteWebJS(inventoryUI, "BURDIGALAX_inventory.setConfig("..json_encode(INVENTORY_CONFIG)..");")
    ShowMouseCursor(true)
    SetInputMode(INPUT_GAMEANDUI)
    SetWebVisibility(inventoryUI, WEB_VISIBLE)
    ExecuteWebJS(inventoryUI, "BURDIGALAX_inventory.show();")
end)

AddRemoteEvent("inventory:update", function(inventoryId, item, quantity, isEquipped)
    local equipped
    if isEquipped then
        equipped = "true"
    else
        equipped = "false"
    end
    AddPlayerChat("Received inventory update: "..inventoryId..", "..item..", "..quantity..", "..equipped)
    ExecuteWebJS(inventoryUI, "BURDIGALAX_inventory.updateItemsInventories("..inventoryId..", [{\"id\": \""..item.."\",\"quantity\":"..quantity..",\"isEquipped\":"..equipped.."}]);")
end)

AddEvent("BURDIGALAX_inventory_onClose", function(context)
    if context == '"transfer"' then
        ExecuteWebJS(inventoryUI, "BURDIGALAX_inventory.updateInventories([{ id: '"..INVENTORY_CONFIG.mainInventoryId.."', selectedNearbyInventoryId: null }])")
        CallRemoteEvent("inventory:close", INVENTORY_CONFIG.inventories[1].selectedNearbyInventoryId)
    else
        inventoryIsOpen = false
        ExecuteWebJS(inventoryUI, "BURDIGALAX_inventory.hide();")
        ShowMouseCursor(false)
        SetInputMode(INPUT_GAME)
        Delay(100, function()
            SetWebVisibility(inventoryUI, WEB_HIDDEN)
        end)
        CallRemoteEvent("inventory:close", INVENTORY_CONFIG.mainInventoryId)
    end
end)

AddEvent("BURDIGALAX_inventory_onEquip", function(event)
    AddPlayerChat("Equiped item ")
    local data = json_decode(event)
    AddRemoteEvent("inventory:equip", data.idInventory, data.idItem)
end)

AddEvent("BURDIGALAX_inventory_onUse", function(event)
    AddPlayerChat("Used item event "..event)
    local data = json_decode(event)
    AddRemoteEvent("inventory:use", data.idInventory, data.idItem)
end)

AddEvent("BURDIGALAX_inventory_onDelete", function(event)
    AddPlayerChat("Dropped item ")
    local data = json_decode(event)
    AddRemoteEvent("inventory:transfer_item", data.idInventory, data.idItem, data.quantity, nil, playerCrouching)
end)

AddEvent("BURDIGALAX_inventory_onTransfer", function(event)
    AddPlayerChat("Transfered item ")
    local data = json_decode(event)
    AddRemoteEvent("inventory:transfer_item", data.originInventoryId,
            data.idItem, data.quantity, data.destinationInventoryId, playerCrouching)
end)

AddEvent("BURDIGALAX_inventory_onChangeNearbyInventorySelected", function()
    AddPlayerChat("Nearby inventory change ")
end)

AddEvent("OnObjectStreamIn", function(object)
    if IsValidObject(object) and GetObjectPropertyValue(object, "collision") == false then
        GetObjectActor(object):SetActorEnableCollision(false)
    end
end)

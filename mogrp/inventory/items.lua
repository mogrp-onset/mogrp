-- Items

-- Types
Inventory.RegisterTypeFunction("food", function(player, item, itemCfg, quantity)
    if itemCfg.hunger then
        AddPlayerHunger(player, quantity * itemCfg.hunger)
    end
    if itemCfg.thirst then
        AddPlayerThirst(player, quantity * itemCfg.thirst)
    end
    if itemCfg.health then
        local health = GetPlayerHealth(player) + (itemCfg.health * quantity)
        if health > 100 then
            health = 100
        end
        SetPlayerHealth(player, health)
    end
    return true
end)

Inventory.RegisterTypeFunction("weapon", function(player, item, itemCfg)
    -- TODO equip weapon
end)

local PLAYER_OBSERVING = {}
local MogInteractiveObjects = ImportPackage("moginteractiveobjects")

local function ObserveInventory(player, inventory)
    inventory:addObserver(player)
    if not PLAYER_OBSERVING[player] then
        PLAYER_OBSERVING[player] = {}
    end
    PLAYER_OBSERVING[inventory.id] = true
end

local function ClearObserverInventory(player, inventoryId)
    GetInventory(inventoryId, function(inventory)
        if inventory then
            inventory:removeObserver(player)
        end
        if not PLAYER_OBSERVING[player] then
            PLAYER_OBSERVING[player] = {}
        end
        PLAYER_OBSERVING[inventoryId] = nil
    end)
end

local function ClearObserver(player)
    if PLAYER_OBSERVING[player] then
        for inventory in pairs(PLAYER_OBSERVING[player]) do
            ClearObserverInventory(player, inventory)
        end
        PLAYER_OBSERVING[player] = nil
    end
end
AddEvent("OnPlayerQuit", ClearObserver)

function OpenInventory(player, title, inventory)
    ObserveInventory(player, inventory)
    CallRemoteEvent(player, "inventory:open", inventory.id, title, inventory.maxWeight)
    for name,item in pairs(inventory.items) do
        SendInventoryUpdate(player, inventory.id, name, item.quantity, item.equipped)
    end
end

AddRemoteEvent("inventory:close", function(player, inventoryId)
    ClearObserverInventory(player, inventoryId)
end)

function SendInventoryUpdate(player, inventoryId, item, quantity, equipped)
    CallRemoteEvent(player, "inventory:update", inventoryId, item, quantity, equipped)
end

function GetInventoryLocation(inventory)
    if inventory.type == "PLAYER" then
        if not IsValidPlayer(inventory.player_id) then
            return nil
        end
        return GetPlayerLocation(inventory.player_id)
    elseif inventory.type == "VEHICLE" then
        return GetVehicleLocation(inventory.vehicle_id)
    elseif inventory.type == "OBJECT" then
        return GetObjectLocation(inventory.object_id)
    elseif inventory.type == "NPC" then
        return GetNPCLocation(inventory.npc_id)
    else
        if inventory.type then
            print("Bad inventory type: "..inventory.type)
        else
            print("Bad inventory type: nil")
        end
        return nil
    end
end

AddRemoteEvent("inventory:transfer_item", function(player, originInventoryId, item, quantity, destinationInventoryId,
                                                   crouching)
    if not ITEMS_CONFIG[item] or not ITEMS_CONFIG[item].droppable then
        NotifyError(player, "Error: Item not droppable")
        return
    end
    GetInventory(originInventoryId, function(inventory)
        if inventory then
            local originX, originY, originZ = GetInventoryLocation(originInventoryId)
            local playerX, playerY, playerZ = GetPlayerLocation(player)
            if originX and GetDistance3D(originX, originY, originZ, playerX, playerY, playerZ) < 500 then
                if inventory.items[item] and inventory.items[item].quantity >= quantity then
                    if destinationInventoryId then
                        GetInventory(destinationInventoryId, function(otherInventory)
                            if otherInventory then
                                local otherX, otherY, otherZ = GetInventoryLocation(otherInventory)
                                if otherX and
                                        GetDistance3D(otherX, otherY, otherZ, playerX, playerY, playerZ) < 500 then
                                    inventory:transferItem(destinationInventoryId, item, quantity, function(status)
                                        if not status then
                                            NotifyError(player, "ERROR: Transfer error")
                                        end
                                    end)
                                else
                                    NotifyError(player, "ERROR: Other inventory not in range")
                                end
                            else
                                NotifyError(player, "ERROR: Other inventory not found")
                            end
                        end)
                    else
                        if inventory:removeItem(item, quantity) then
                            local x, y, z = GetPlayerLocation(player)
                            if crouching then
                                z = z + 40
                            end
                            local h = GetPlayerHeading(player)
                            SetPlayerAnimation(player, "STOP")
                            SetPlayerAnimation(player, "CARRY_SHOULDER_SETDOWN")
                            Delay(1000, function()
                                if not CreateDropObject(item, quantity, x, y, z - 95, 90, h - 90) then
                                    NotifyError(player, "DROP_ERROR")
                                end
                            end)
                        else
                            NotifyError(player, "ERROR: Remove error")
                        end
                    end
                else
                    NotifyError(player, "ERROR: You don't have that many items !")
                end
            else
                NotifyError(player, "ERROR: Inventory not in range")
            end
        else
            NotifyError(player, "ERROR: Inventory not found")
        end
    end)
end)

AddRemoteEvent("inventory:use", function(player, inventoryId, item)
    GetInventory(inventoryId, function(inventory)
        if inventory then
            local originX, originY, originZ = GetInventoryLocation(originInventoryId)
            local playerX, playerY, playerZ = GetPlayerLocation(player)
            if originX and GetDistance3D(originX, originY, originZ, playerX, playerY, playerZ) < 500 then
                if not inventory:useItem(player, item, 1) then
                    AddPlayerChat(player, "ERROR: Cannot use this item")
                end
            else
                AddPlayerChat(player, "ERROR: Inventory not in range")
            end
        else
            AddPlayerChat(player, "ERROR: Inventory not found")
        end
    end)
end)

-- TODO Implement equip
AddRemoteEvent("inventory:equip", function(player, inventoryId, item)
    AddPlayerChat(player, "NOT IMPLEMENTED YET")
end)

function CreateDropObject(item, quantity, x, y, z, rx, ry, rz, sx, sy, sz)
    local itemConfig = ITEMS_CONFIG[item]
    local droppedObject = CreateObject(itemConfig.model, x, y, z, rx, ry, rz, sx, sy, sz)
    SetObjectPropertyValue(droppedObject, "collision", false, true)
    SetObjectPropertyValue(droppedObject, "item", item, true)
    SetObjectPropertyValue(droppedObject, "quantity", quantity, true)
    local items = {}
    items[item] = quantity
    if not gathering.Register(droppedObject, {
            lootTable = {
                {
                    items = items
                }
            },
            clearOnFullLoot = true
        }) then
        DestroyObject(droppedObject)
        return false
    end
    return droppedObject
end

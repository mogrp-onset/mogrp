INVENTORY_CONFIG = {
  items = {
    id = "test",
    name = "test",
    categoryId = 1,
    description = "",
    health = null,
    imageUrl = null,
    isEquipable = false,
    isDeletable = true,
    isUsable = true,
    iconUrl = null,
    weight = 0.5,
    effects = {
      {
        id = "food",
        value = 3
      }
    }
  },
  effects = {
    hydration = {
      name = "Hydratation",
      unit = "%",
      iconUrl = "data:image/svg+xml,%0A%3Csvg width='28' height='28' viewBox='0 0 28 28' xmlns='http://www.w3.org/2000/svg'%3E%3Ctitle%3Eeffet-hydrat@1x%3C/title%3E%3Cdesc%3ECreated with Sketch.%3C/desc%3E%3Cg transform='translate(1 1)' fill='none'%3E%3Cg fill='%236ABEE4' fill-rule='nonzero'%3E%3Cpath d='M12.963 22c-3.828 0-6.943-3.114-6.943-6.943 0-3.579 5.711-10.915 6.363-11.741.14-.177.354-.281.58-.281.226 0 .44.103.58.281.651.825 6.363 8.16 6.363 11.741 0 3.828-3.114 6.943-6.942 6.943zm0-17.013c-1.933 2.564-5.465 7.733-5.465 10.07 0 3.013 2.451 5.464 5.465 5.464 3.013 0 5.465-2.451 5.465-5.464 0-2.337-3.532-7.506-5.465-10.07z'/%3E%3Cpath d='M12.963 19.045c-.389-.026-.691-.349-.691-.739s.302-.713.691-.739c1.385-.001 2.507-1.124 2.509-2.509.026-.389.349-.691.739-.691s.713.302.739.691c-.003 2.201-1.787 3.984-3.987 3.987z'/%3E%3C/g%3E%3Ccircle stroke='%236ABEE4' stroke-width='2' cx='13' cy='13' r='13'/%3E%3C/g%3E%3C/svg%3E"
    },
    food = {
      name = "Alimentation",
      iconUrl = "data:image/svg+xml,%0A%3Csvg width='28' height='28' viewBox='0 0 28 28' xmlns='http://www.w3.org/2000/svg'%3E%3Ctitle%3Eeffet-food@1x%3C/title%3E%3Cdesc%3ECreated with Sketch.%3C/desc%3E%3Cg transform='translate(1 1)' fill='none'%3E%3Cg fill='%237F95A5' fill-rule='nonzero'%3E%3Cpath d='M17.351 5.092c-.479 0-.912.193-1.286.574-1.838 1.87-1.786 8.214-1.774 8.931l.009.52h2.523v6.296c0 .292.236.528.528.528.292 0 .528-.237.528-.528v-15.792c0-.292-.236-.528-.528-.528zM12.317 9.929h-.71v-4.87h-.698v4.87h-.71v-4.87h-.698v4.87h-.71v-4.87h-.633v4.864c0 1.119.769 2.057 1.806 2.32l-.508 9.01c0 .38.789.688 1.097.688.309 0 1.097-.308 1.097-.688l-.508-9.01c1.037-.263 1.806-1.2 1.806-2.32v-4.864h-.633v4.87h.001z'/%3E%3C/g%3E%3Ccircle stroke='%237F95A5' stroke-width='2' cx='13' cy='13' r='13'/%3E%3C/g%3E%3C/svg%3E",
      unit = "%"
    },
    heal = {
      name = "Vie",
      iconUrl = "data:image/svg+xml,%0A%3Csvg width='28' height='28' viewBox='0 0 28 28' xmlns='http://www.w3.org/2000/svg'%3E%3Ctitle%3Eeffet-food@1x%3C/title%3E%3Cdesc%3ECreated with Sketch.%3C/desc%3E%3Cg transform='translate(1 1)' fill='none'%3E%3Cg fill='%237F95A5' fill-rule='nonzero'%3E%3Cpath d='M17.351 5.092c-.479 0-.912.193-1.286.574-1.838 1.87-1.786 8.214-1.774 8.931l.009.52h2.523v6.296c0 .292.236.528.528.528.292 0 .528-.237.528-.528v-15.792c0-.292-.236-.528-.528-.528zM12.317 9.929h-.71v-4.87h-.698v4.87h-.71v-4.87h-.698v4.87h-.71v-4.87h-.633v4.864c0 1.119.769 2.057 1.806 2.32l-.508 9.01c0 .38.789.688 1.097.688.309 0 1.097-.308 1.097-.688l-.508-9.01c1.037-.263 1.806-1.2 1.806-2.32v-4.864h-.633v4.87h.001z'/%3E%3C/g%3E%3Ccircle stroke='%237F95A5' stroke-width='2' cx='13' cy='13' r='13'/%3E%3C/g%3E%3C/svg%3E",
      unit = "%"
    }
  },
  categories = {
    {
      id = 1,
      name = "Consommables",
      iconUrl = "data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='12' height='16' viewBox='0 0 12 16'%3E%3Ctitle%3Eicon-filter-food%3C/title%3E%3Cdesc%3ECreated with Sketch.%3C/desc%3E%3Cpath d='M4.347 7.042l-.928.56.708 6.65c.048.605-.248 1.185-.765 1.502-.517.317-1.169.317-1.686 0-.517-.317-.813-.898-.765-1.502l.708-6.65-.928-.56c-.443-.266-.707-.752-.69-1.268l.167-5.333c.01-.248.215-.443.463-.44.247 0 .449.195.458.442l.12 3.478c.003.19.145.349.333.373.101.011.203-.02.28-.087.077-.067.123-.163.127-.265l.12-3.482c0-.253.205-.458.458-.458s.458.205.458.458l.12 3.482c.004.1.048.194.123.26s.173.099.272.092c.189-.024.331-.183.333-.373l.117-3.478c.009-.246.211-.441.457-.442.247 0 .449.195.458.442l.167 5.333c.018.515-.244 1-.685 1.267zm7.32-2.983c0-2.242-1.137-4.058-2.537-4.058s-2.537 1.817-2.537 4.058c0 1.717.667 3.167 1.613 3.758l-.685 6.435c-.048.605.248 1.185.765 1.502.517.317 1.169.317 1.686 0 .517-.317.813-.898.765-1.502l-.685-6.435c.947-.592 1.613-2.042 1.613-3.758z' fill='%23000' fill-rule='nonzero'/%3E%3C/svg%3E"
    }
  }
}

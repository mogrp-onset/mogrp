GATHER_CONFIGS = {
    wheat = {
        neededItem = "scythe",
        animation = nil,
        delay = nil,
        lootTable = {
            {
                weight = 99,
                lootTable = nil,
                items = {
                    wheat = 1
                }
            },
            {
                weight = 1,
                lootTable = nil,
                items = {
                    wheat = 2
                }
            }
        }
    }
}

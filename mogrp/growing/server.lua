local growing = {}

local growings = {}
local id = 0

local function nextStep(data)
    data.currentStep = data.currentStep + 1
    if data.lastObjectId then
        if not IsValidObject(data.lastObjectId) then
            return
        end
        DestroyObject(data.lastObjectId)
    end
    local modelData = rawget(data.stepModels, data.currentStep)
    data.lastObjectId = CreateObject(
            modelData.model,
            data.x,
            data.y,
            data.z,
            data.rx,
            data.ry,
            data.rz,
            modelData.sx or 1,
            modelData.sy or 1,
            modelData.sz or 1
    )
    if data.currentStep < data.steps then
        Delay(data.stepDuration, function()
            nextStep(data)
        end)
    else
        growing.Unregister(data.id, true)
        if not gathering.Register(data.lastObjectId, data.config.gatherConfig) then
            print("ALERT: Error when registering gatherable, destroying growing object "..data.lastObjectId)
            DestroyObject(data.lastObjectId)
        end
    end
end

function growing.Register(growConfig, x, y, z, rx, ry, rz)
    id = id + 1
    local stepDuration = growConfig.timeToGrow / #growConfig.stepModels
    local data = {
        id = id,
        x = x,
        y = y,
        z = z,
        rx = rx,
        ry = ry,
        rz = rz,
        config = growConfig,
        stepDuration = stepDuration,
        currentStep = 0,
        steps = #growConfig.stepModels
    }
    rawset(growings, id, data)
    nextStep(data)
    return id
end

function growing.Unregister(id, dontDestroy)
    local data = rawget(growings, id)
    if data then
        rawset(growings, id, nil)
        if not dontDestroy and data.lastObjectId then
            DestroyObject(data.lastObjectId)
        end
    end
end

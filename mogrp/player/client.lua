playerCrouching = false

function PlayerHandsFree()
    return not GetPlayerPropertyValue(GetPlayerId(), "cuffed")
end

AddEvent("OnKeyPress", function(key)
    if key == "I" and not inventoryIsOpen then
        CallRemoteEvent("player:inventory:open")
    end
end)

AddEvent("OnPlayerCrouch", function()
    playerCrouching = true
end)

AddEvent("OnPlayerEndCrouch", function()
    playerCrouching = false
end)

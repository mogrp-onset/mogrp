PLAYER_SPAWN_POINT = { x = 204094, y = 180846, z = 1500, h = 0 }
HOSPITAL_SPAWN_POINT = { x = 212105, y = 159446, z = 1515, h = 90 }

AddEvent("OnPackageStart", function()
    CreateTimer(function()
        print("Saving player positions...")
        for _,player in pairs(GetAllPlayers()) do
            SavePlayer(player)
        end
    end, 30000)
end)

function OnPlayerJoin(player)
    SetPlayerSpawnLocation(player, 227603, -65590, 400, 90)
end
AddEvent("OnPlayerJoin", OnPlayerJoin)

function OnPlayerSteamAuth(player)
    GetPlayer(player, function(playerData)
        if playerData then
            SetPlayerSpawnLocation(player, HOSPITAL_SPAWN_POINT.x, HOSPITAL_SPAWN_POINT.y, HOSPITAL_SPAWN_POINT.z,
                    HOSPITAL_SPAWN_POINT.h)
            -- Update skin
            UpdateClothes(player)
            -- Retrieve last position data
            GetPlayerProperty(player, "position", function(data)
                local location = PLAYER_SPAWN_POINT
                if data then
                    location = data
                end
                SetPlayerLocation(player, location.x, location.y, location.z + 250)
                SetPlayerHeading(player, location.h)
            end)
            GetPlayerProperty(player, "hunger", function(data)
                SetPlayerHunger(player, data or 100)
            end)
            GetPlayerProperty(player, "thirst", function(data)
                SetPlayerThirst(player, data or 100)
            end)
            GetPlayerProperty(player, "health", function(data)
                SetPlayerHealth(player, data or 100)
            end)
        end
    end)
end
AddEvent("OnPlayerSteamAuth", OnPlayerSteamAuth)

function SavePlayer(player)
    GetPlayer(player, function (data)
        if data then
            local x, y, z = GetPlayerLocation(player)
            local h = GetPlayerHeading(player)
            SetPlayerProperty(player, "position", {
                x = x,
                y = y,
                z = z,
                h = h
            }, function() end)
            SetPlayerProperty(player, "health", GetPlayerHealth(player), function() end)
        end
    end)
end
AddEvent("OnPlayerQuit", SavePlayer)

function PlayerHandsFree(player)
    return not GetPlayerPropertyValue(player, "cuffed")
end

-- Player inventory
function GetPlayerInventory(player, fnc)
    GetPlayer(player, function(playerData)
        if playerData then
            GetPlayerProperty(player, "inventory", function(inventoryId)
                if not inventoryId then
                    Inventory:create({
                        type = "PLAYER",
                        player_id = player
                    }, function(inventory)
                        SetPlayerProperty(player, "inventory", inventory.id, function() end)
                        fnc(inventory)
                    end)
                else
                    GetInventory(inventoryId, function(inventory)
                        inventory.player_id = player
                        inventory.type = "PLAYER"
                        fnc(inventory)
                    end)
                end
            end)
        else
            fnc(nil)
        end
    end)
end

local function OpenPlayerInventory(player, playerInventoryToOpen)
    if IsValidPlayer(player) then
        GetPlayer(playerInventoryToOpen, function(playerData)
            GetPlayerInventory(playerInventoryToOpen, function(data)
                if data then
                    OpenInventory(player, playerData.name.."'s inventory", data)
                else
                    AddPlayerChat(player, "Cannot open inventory :(")
                end
            end)
        end)
    else
        AddPlayerChat(player, "Not a player")
    end
end

AddRemoteEvent("player:inventory:open", function(player, playerInventoryToOpen)
    if playerInventoryToOpen then
        GetPlayer(player, function(data)
            if data and data.rights >= 2000 then
                OpenPlayerInventory(player, playerInventoryToOpen)
            end
        end)
    else
        OpenPlayerInventory(player, player)
    end
end)

-- Commands
RegisterCommand("playeradditem", 2000, function(player, playerToAdd, item, quantity)
    if not playerToAdd or not item or not quantity then
        AddPlayerChat("Usage: /additem <player> <item> <quantity>")
        return
    end
    if not IsValidPlayer(playerToAdd) then
        AddPlayerChat(player, "NOT_PLAYER")
        return
    end
    if not ITEMS_CONFIG[item] then
        AddPlayerChat(player, "NOT_ITEM")
        AddPlayerChat(player, "MUST_BE: "..table.concat(ITEMS_CONFIG, ", "))
        return
    end
    if quantity == 0 then
        AddPlayerChat(player, "BAD_QUANTITY")
        return
    end
    GetPlayerInventory(playerToAdd, function(inventory)
        if not inventory then
            AddPlayerChat(player, "INVENTORY_ERROR")
            return
        end
        if quantity > 0 then
            inventory:addItem(item, quantity)
        else
            inventory:removeItem(item, quantity)
        end
        AddPlayerChat(player, "Added item to player's inventory !")
    end)
end)

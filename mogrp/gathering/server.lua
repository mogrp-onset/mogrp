gathering = {}

local gatherables = {}

function gathering.Register(objectId, gatherConfig)
    if MogInteractiveObjects.RegisterInteractiveObject(5, objectId) then
        rawset(gatherables, objectId, gatherConfig)
        return true
    end
    print("ALERT: Cannot register gathering interaction: "..objectId)
    return false
end

function gathering.Unregister(objectId)
    rawset(gatherables, objectId, nil)
end

local function Gather(player, objectId, gatherConfig, inventory)
    if gatherConfig.gathered then
        return -- Already gathered, used to avoid racing conditions
    end
    SetPlayerAnimation(player, "STOP")
    SetPlayerAnimation(player, gatherConfig.animation or "PICKUP_LOWER")
    Delay(gatherConfig.delay or 1000, function()
        SetPlayerAnimation(player, "STOP")
        local lootTable = gatherConfig.lootTable
        local fullLoot = true
        repeat
            local loot = lootTable[1]
            if #gatherConfig.lootTable > 1 then
                loot = getWeightedRandom(lootTable)
            end
            lootTable = loot.lootTable
            if loot.items then
                for item, quantity in pairs(loot.items) do
                    if not inventory.addItem(item, quantity) then
                        NotifyError(player, "Error: Cannot add item "..item.."x"..quantity..": Inventory full")
                        fullLoot = false
                    end
                end
            end
        until not loot.lootTable
        if fullLoot or not gatherConfig.clearOnFullLoot then
            gathering.Unregister(objectId)
            DestroyObject(objectId)
        end
    end)
end

AddEvent("InteractiveObjectInteract", function(player, objectType, objectId)
    local gatherConfig = rawget(gatherables, objectId)
    if objectType == 5 and gatherConfig then
        gatherConfig.gathered = true
        GetPlayerInventory(player, function(inventory)
            if gatherConfig.neededItem then
                    local item = inventory.getItem(gatherConfig.neededItem)
                    if item and item.equipped then
                        Gather(player, objectId, gatherConfig, inventory)
                    else
                        NotifyError(player, "Error: Item needed: "..item)
                    end
            else
                Gather(player, objectId, gatherConfig, inventory)
            end
        end)
    end
end)

function Notify(player, text, color, duration)
    if not color then
        color = "linear-gradient(to right, #00b09b, #96c93d)"
    end
    if not duration then
        duration = 5000
    end
    CallRemoteEvent(player, "MakeNotification", text, color, duration)
end

function NotifySuccess(player, text, duration)
    if not duration then
        duration = 5000
    end
    CallRemoteEvent(player, "MakeSuccessNotification", text, duration)
end

function NotifyError(player, text, duration)
    if not duration then
        duration = 5000
    end
    CallRemoteEvent(player, "MakeErrorNotification", text, duration)
end

shirtsModel = {
    formal_shirt_1 = "/Game/CharacterModels/SkeletalMesh/Outfits/HZN_Outfit_Piece_FormalShirt_LPR",
    formal_shirt_2 = "/Game/CharacterModels/SkeletalMesh/Outfits/HZN_Outfit_Piece_FormalShirt2_LPR",
    simple_shirt = "/Game/CharacterModels/SkeletalMesh/Outfits/HZN_Outfit_Piece_Shirt_LPR",
    knitted_shirt_2 = "/Game/CharacterModels/SkeletalMesh/Outfits/HZN_Outfit_Piece_TShirt_Knitted2_LPR",
    knitted_shirt_1 = "/Game/CharacterModels/SkeletalMesh/Outfits/HZN_Outfit_Piece_TShirt_Knitted_LPR",
    tshirt = "/Game/CharacterModels/SkeletalMesh/Outfits/HZN_Outfit_Piece_TShirt_LPR",
}

pantsModel = {
    cargo_pants = "/Game/CharacterModels/SkeletalMesh/Outfits/HZN_Outfit_Piece_CargoPants_LPR",
    denim_pants = "/Game/CharacterModels/SkeletalMesh/Outfits/HZN_Outfit_Piece_DenimPants_LPR",
    formal_pants = "/Game/CharacterModels/SkeletalMesh/Outfits/HZN_Outfit_Piece_FormalPants_LPR"
}

shoesModel = {
    normal_shoes = "/Game/CharacterModels/SkeletalMesh/Outfits/HZN_Outfit_Piece_NormalShoes_LPR",
    business_shoes = "/Game/CharacterModels/SkeletalMesh/Outfits/HZN_Outfit_Piece_BusinessShoes_LPR"
}

hairsModel = {
    hairs_business = "/Game/CharacterModels/SkeletalMesh/HZN_CH3D_Hair_Business_LP",
    hairs_scientist = "/Game/CharacterModels/SkeletalMesh/HZN_CH3D_Hair_Scientist_LP",
    hairs_1 = "/Game/CharacterModels/SkeletalMesh/HZN_CH3D_Normal_Hair_01_LPR",
    hairs_3 = "/Game/CharacterModels/SkeletalMesh/HZN_CH3D_Normal_Hair_03_LPR",
    hairs_2 = "/Game/CharacterModels/SkeletalMesh/HZN_CH3D_Normal_Hair_02_LPR"
}

hairsColor = {
    blond = { 250, 240, 190, 1 },
    light_blond = { 222, 190, 153, 1 },
    dark = { 36, 28, 17, 1 },
    brown = { 139, 69, 19, 1 },
    redhead = { 154, 51, 0, 1 },
    grey = { 169, 169, 169, 1 },
    light_grey = { 211, 211, 211, 1},
    black = { 9, 8, 6, 1 }
}

skinColor = {
    skin_1 = { 0.9, 0.9, 0.9, 0 },
    skin_2 = { 0.7, 0.7, 0.7, 0 },
    skin_3 = { 0.5, 0.5, 0.5, 0 },
    skin_4 = { 0.3, 0.3, 0.3, 0 },
    skin_5 = { 0.1, 0.1, 0.1, 0 },
    skin_6 = { 0.05, 0.05, 0.05, 0 }
}

defaultCharacter = {
    -- faces = 'sdfadf1',
    hair = 'hairs_business',
    haircolors = 'dark',
    skins = 'skin_1',
    tops = 'knitted_shirt_2',
    -- jackets = 'sdfadf2',
    trousers = 'formal_pants',
    shoes = 'normal_shoes'
}

function CheckCharacterStatus(player)
    GetPlayer(player, function(data)
        if not data then
            local color = hairsColor[defaultCharacter.haircolors]
            local skin = skinColor[defaultCharacter.skins]
            CallRemoteEvent(player, "ClientChangeClothing", player, 0, hairsModel[defaultCharacter.hair], color[1], color[2], color[3], color[4])
            CallRemoteEvent(player, "ClientChangeClothing", player, 1, shirtsModel[defaultCharacter.tops], 0, 0, 0, 0)
            CallRemoteEvent(player, "ClientChangeClothing", player, 4, pantsModel[defaultCharacter.trousers], 0, 0, 0, 0)
            CallRemoteEvent(player, "ClientChangeClothing", player, 5, shoesModel[defaultCharacter.shoes], 0, 0, 0, 0)
            CallRemoteEvent(player, "ClientChangeClothing", player, 6, "noShoesLegsTorso", skin[1], skin[2], skin[3], skin[4])

            Delay(2000, function ()
                CallRemoteEvent(player, "charactercreator:open")
            end)
        end
    end)
end
AddRemoteEvent("charactercreator:check_character_status", CheckCharacterStatus)


AddRemoteEvent("charactercreator:submit", function(player, firstName, lastName, age)
    local name = firstName.." "..lastName
    UpdatePlayer(player, {
        name = name,
        phone_number = "666"..generateRandomNumberString(7)
    }, function()
        SetPlayerName(player, name)
        AddPlayerChat(player, "Profile created !")
        SetPlayerProperty(player, "age", age, function()
            AddPlayerChat(player, "Age updated !")
        end)
        CallRemoteEvent(player, "charactereditor:open", defaultCharacter, true)
    end)
end)

AddEvent("OnPlayerSpawn", function(player)
    UpdateClothes(player)
end)

function ChangeOtherPlayerClothes(player, otherPlayer)
    GetPlayerProperty(otherPlayer, "clothing", function(data)
        if data then
            SendPlayerClothesData(otherPlayer, player, data)
        end
    end)
end
AddRemoteEvent("ServerChangeOtherPlayerClothes", ChangeOtherPlayerClothes)

function UpdateClothes(player)
    GetPlayerProperty(player, "clothing", function(data)
        if data then
            SendPlayerClothesData(player, player, data)
            for k, v in pairs(GetStreamedPlayersForPlayer(player)) do
                SendPlayerClothesData(player, v, data)
            end
        end
    end)
end

function SendPlayerClothesData(player, sendTo, data)
    local skin = skinColor[data.skin_color]
    CallRemoteEvent(sendTo, "ClientChangeClothing", player, 6, "noShoesLegsTorso",
            skin[1], skin[2], skin[3], skin[4])

    local playerHairColor = hairsColor[data.hair_color]

    CallRemoteEvent(sendTo, "ClientChangeClothing", player, 0, hairsModel[data.hair],
            playerHairColor[1], playerHairColor[2], playerHairColor[3], playerHairColor[4])
    CallRemoteEvent(sendTo, "ClientChangeClothing", player, 1, shirtsModel[data.shirt], 0, 0, 0, 0)
    CallRemoteEvent(sendTo, "ClientChangeClothing", player, 4, pantsModel[data.pants], 0, 0, 0, 0)
    CallRemoteEvent(sendTo, "ClientChangeClothing", player, 5, shoesModel[data.shoes], 0, 0, 0, 0)
end

function RegisterRemoteEvent(remoteEvent, minRights, fnc)
    print("[CommandsManager] Registering remote event "..remoteEvent)
    AddRemoteEvent(remoteEvent, function(playerId, ...)
        GetPlayer(playerId, function(player, ...)
            if not player or tonumber(player.rights) < minRights then
                AddPlayerChat(playerId, "ERROR: You are not authorized to execute this remote event.")
            else
                fnc(playerId, ...)
            end
        end, ...)
    end)
end
AddFunctionExport("RegisterRemoteEvent", RegisterRemoteEvent)

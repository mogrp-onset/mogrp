local databases = {}

AddEvent("OnPackageStart", function()
    local config = json_load("databases.json")

    if config == nil then
        print("Database config file not found.")
        ServerExit()
        return
    end

    mariadb_log(config['logLevel'])

    for key, value in pairs(config['databases']) do
        local port = 3306
        if value['port'] ~= nil then
            port = tonumber(value['port'])
        end
        local handle = mariadb_connect(value['host']..':'..port, value['user'], value['password'],
                value['database'])
        if (handle ~= false) then
            print("MariaDB: Connected to " .. value['host'])
            local charset = 'utf8'
            if value['charset'] ~= nil then
                charset = value['charset']
            end
            mariadb_set_charset(handle, charset)
        else
            print("MariaDB: Connection failed to "..value['host']..", see mariadb_log file")
            ServerExit()
            return
        end
        databases[key] = handle
    end
end)

function GetDbHandle(name)
    return databases[name]
end

local function OnQueryError(errorid, error_str, query_str, handle_id)
    print("DATABASE ERROR: "..error_str)
end
AddEvent("OnQueryError", OnQueryError)

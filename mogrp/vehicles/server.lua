--local quadTree = QuadTree.new(-100000, -1000000, 4000000, 4000000, 20)
local vehicles = Map:new()

local FUEL_CONSUMPTION_BASE = .01
local FUEL_SPEED_MODIFIER = .01
local handle

local function GetFuelValue(vehicleData)
    local vehicleConfig = GetVehicleConfig(tonumber(vehicleData.model_id))
    if vehicleData.fuel > vehicleConfig.maxFuel then
        vehicleData.fuel = vehicleConfig.maxFuel
    end
    return (vehicleData.fuel / vehicleConfig.maxFuel) * 100.0
end

AddEvent("OnPackageStart", function()
    handle = GetDbHandle("general")

    -- Load all vehicles from database
    mariadb_async_query(handle, mariadb_prepare(handle,
            "SELECT * FROM vehicles"
    ), function()
        for i = 1, mariadb_get_row_count() do
            AddVehicle(mariadb_get_assoc(i))
        end
    end)

    CreateTimer(function()
        print("Saving vehicles...")
        for key,value in pairs(vehicles:items()) do
            SaveVehicle(key)
        end
    end, 30000)

    -- Fuel tick system
    CreateTimer(function()
        for k,v in pairs(vehicles:items()) do
            if GetVehicleEngineState(k) then
                v.fuel = tonumber(v.fuel)
                local vX, vY, vZ = GetVehicleVelocity(k)
                local speed = math.exp((math.sqrt(vX * vX + vY * vY + vZ * vZ) / 30) / 150) - 1
                if v.fuel > 0 then
                    v.fuel = v.fuel - (FUEL_CONSUMPTION_BASE + (speed * FUEL_SPEED_MODIFIER))
                else
                    StopVehicleEngine(k)
                    v.fuel = 0
                end
                SetVehiclePropertyValue(k, "fuel", GetFuelValue(v), true)
                v.fuel = tostring(v.fuel)
            end
        end
    end, 1000)
end)

function GetVehicleData(vehicle)
    local vehicleData = vehicles:get(vehicle)
    if not vehicleData then
        return nil
    end
    return vehicleData, GetVehicleConfig(tonumber(vehicleData.model_id))
end

function GetVehicleConfig(modelId)
    local vehicleConfig = VEHICLE_CONFIGS[modelId]
    if not vehicleConfig then
        vehicleConfig = DEFAULT_VEHICLE_CONFIG
    end
    return vehicleConfig
end

function AddVehicle(data)
    local vehicle = CreateVehicle(tonumber(data['model_id']),
            tonumber(data['x']), tonumber(data['y']), tonumber(data['z']), tonumber(data['h']))
    SetVehicleLicensePlate(vehicle, data['license_plate'])
    SetVehicleHealth(vehicle, tonumber(data['health']))
    SetVehicleRespawnParams(vehicle, false)
    SetVehicleColor(vehicle, RGB(tonumber(data['color_r']), tonumber(data['color_g']), tonumber(data['color_b'])))
    SetVehiclePropertyValue(vehicle, "locked", true, true)
    data.fuel = tonumber(data.fuel)
    SetVehiclePropertyValue(vehicle, "fuel", GetFuelValue(data), true)
    data.fuel = tostring(data.fuel)
    -- TODO Set vehicle damages ?
    vehicles:put(vehicle, data)
    return vehicle
end

function CreatePersistentVehicle(modelId, x, y, z, h)
    AddVehicle({
        model_id = modelId,
        x = x,
        y = y,
        z = z,
        h = h,
        fuel = 100,
        key_code = math.random(0,999999999),
        color_r = 0,
        color_g = 0,
        color_b = 0,
        health = 5000,
        license_plate = generateLicensePlate()
    })
end

RegisterCommand("createvehicle", 3000, function(player, modelId, x, y, z, h)
    if x and y and z and h then
        CreatePersistentVehicle(modelId, x, y, z, h)
    elseif (modelId) then
        local x, y, z = GetPlayerLocation(player)
        local h = GetPlayerHeading(player)
        CreatePersistentVehicle(modelId, x, y, z, h)
    else
        AddPlayerChat(player, "Usage: /createvehicle <modelId> <x> <y> <z> <h>")
    end
end)

function SaveVehicle(vehicle)
    local data = vehicles:get(vehicle)
    local x, y, z = GetVehicleLocation(vehicle)
    local h = GetVehicleHeading(vehicle)
    local health = GetVehicleHealth(vehicle)
    local model = GetVehicleModel(vehicle)
    data.x = x
    data.y = y
    data.z = z
    data.h = h
    data.health = health
    data.model_id = model
    if data.id and data.owner_steam_id then
        mariadb_async_query(handle, mariadb_prepare(handle,
                "REPLACE INTO vehicles (id, owner_steam_id, model_id, x, y, z, h, fuel, key_code, color_r, color_g, color_b, health, license_plate) VALUES (?, '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?')",
                data.id,
                data.owner_steam_id,
                data.model_id,
                data.x,
                data.y,
                data.z,
                data.h,
                data.fuel,
                data.key_code,
                data.color_r,
                data.color_g,
                data.color_b,
                data.health,
                data.license_plate
        ), function() end)
    elseif data.id then
        mariadb_async_query(handle, mariadb_prepare(handle,
                "REPLACE INTO vehicles (id, model_id, x, y, z, h, fuel, key_code, color_r, color_g, color_b, health, license_plate) VALUES (?, '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?')",
                data.id,
                data.model_id,
                data.x,
                data.y,
                data.z,
                data.h,
                data.fuel,
                data.key_code,
                data.color_r,
                data.color_g,
                data.color_b,
                data.health,
                data.license_plate
        ), function() end)
    elseif data.owner_steam_id then
        mariadb_async_query(handle, mariadb_prepare(handle,
                "REPLACE INTO vehicles (owner_steam_id, model_id, x, y, z, h, fuel, key_code, color_r, color_g, color_b, health, license_plate) VALUES ('?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?')",
                data.owner_steam_id,
                data.model_id,
                data.x,
                data.y,
                data.z,
                data.h,
                data.fuel,
                data.key_code,
                data.color_r,
                data.color_g,
                data.color_b,
                data.health,
                data.license_plate
        ), function()
            data.id = mariadb_get_insert_id()
        end)
    else
        mariadb_async_query(handle, mariadb_prepare(handle,
                "REPLACE INTO vehicles (model_id, x, y, z, h, fuel, key_code, color_r, color_g, color_b, health, license_plate) VALUES ('?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?')",
                data.model_id,
                data.x,
                data.y,
                data.z,
                data.h,
                data.fuel,
                data.key_code,
                data.color_r,
                data.color_g,
                data.color_b,
                data.health,
                data.license_plate
        ), function()
            data.id = mariadb_get_insert_id()
        end)
    end
end

--[[AddEvent("OnPlayerLeaveVehicle", function(player, vehicle, seat)
    if seat == 1 then -- Player is the conductor, so the vehicle can be considered as stopped
        SaveVehicle(vehicle)
    end
end)]]

AddRemoteEvent("vehicle:unlock", function(player, vehicle)
    if IsValidVehicle(vehicle) then
        local x, y, z = GetVehicleLocation(vehicle)
        local x2, y2, z2 = GetPlayerLocation(player)
        if GetDistance3D(x, y, z, x2, y2, z2) < 300 then
            local lockValue = not GetVehiclePropertyValue(vehicle, "locked")
            SetVehiclePropertyValue(vehicle, "locked", lockValue, true)
            if lockValue then
                NotifySuccess(player, "Vehicle locked")
            else
                NotifySuccess(player, "Vehicle unlocked")
            end
        end
    end
end)

AddRemoteEvent("vehicle:toggle_engine", function(player)
    local vehicle = GetPlayerVehicle(player)
    if vehicle == 0 then
        NotifyError(player, "Not in a vehicle")
    elseif GetPlayerVehicleSeat(player) ~= 1 then
        NotifyError(player, "You must be the driver to do this")
    else
        local vehicleEngineState = GetVehicleEngineState(vehicle)
        if vehicleEngineState then
            StopVehicleEngine(vehicle)
        else
            StartVehicleEngine(vehicle)
        end
    end
end)

-- Imported from OnsetRP
function generateLicensePlate()
    local plate = ""
    -- FIRST 2 LETTERS
    plate = plate .. generateRandomString(2) .. "-"
    -- 3 MIDDLE NUMBERS
    plate = plate .. generateRandomNumberString(3) .. "-"
    -- LAST 2 LETTERS
    plate = plate .. generateRandomString(2)
    return plate
end

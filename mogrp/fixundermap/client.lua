-- inspired from https://github.com/Kuzkay/OkayyNetwork/blob/master/OKAYY%20Framework%20Basic/kuz_Undermap/client.lua

function OnPackageStart()
    Delay(5000, function()
        CreateTimer(Check, 500)
        Check()
    end)
end
AddEvent("OnPackageStart", OnPackageStart)

function Check()
    local x, y, z = GetPlayerLocation()

    local terrain = GetTerrainHeight(x, y, 99999.9)
    if z < 0 and terrain - 400 > z and not IsPlayerInVehicle() then
        GetPlayerActor(GetPlayerId()):SetActorLocation(x, y, terrain + 200)
    end
end

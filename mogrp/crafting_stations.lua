CRAFTING_STATIONS = {
    cookingStation = {
        models = {}, -- List of models
        jobs = {"cook"}, -- List of jobs that are authorized to use this crafting station
        crafts = { -- List of crafts
            pie = { -- Item = Quantity
                apple = 5,
                pie_paste = 1
            }
        }
    }
}

local craftingStations = {}

AddEvent("WorldObjectLoaded", function(objectId, data)
    if data['modelID'] ~= nil and data['specialObjectData']
            and data['specialObjectData']['type'] == "craftingStation" then
        if MogInteractiveObjects.RegisterInteractiveObject(5, objectId) then
            local craftingStation = data['specialObjectData']['data']['station']
            if CRAFTING_STATIONS[craftingStation] then
                rawset(craftingStations, objectId, craftingStation)
                --                SetObjectPropertyValue(objectId, "craftingStation", craftingStation, true)
                print("CraftingStationRegistered: "..objectId.." !")
            else
                print("No data for this crafting stations ?!")
            end
        else
            print("CraftingStationCannotBeRegistered: "..objectId)
        end
    end
end)

AddEvent("InteractiveObjectInteract", function(player, objectType, objectId)
    local craftingStation = craftingStations[objectId]
    if objectType == 5 and craftingStation then
        CallRemoteEvent(player, "crafting:open", craftingStation)
    end
end)

local function Craft(player, station, recipeItem, craftQuantity)
    local pX, pY, pZ = GetPlayerLocation(player)
    if not IsValidObject(station) then
        return
    end
    local oX, oY, oZ = GetObjectLocation(station)
    if GetDistance3D(pX, pY, pZ, oX, oY, oZ) < 500 then
        local craftingStation = rawget(craftingStations, station)
        if craftingStation then
            local craftingStationConfig = rawget(CRAFTING_STATIONS, craftingStation)
            if craftingStationConfig then
                -- TODO Check job
                local recipe = rawget(craftingStationConfig.crafts, recipeItem)
                if recipe then
                    GetPlayerInventory(player, function(inventory)
                        local validRecipe = true
                        for item,quantity in pairs(recipe) do
                            quantity = quantity * craftQuantity
                            local playerItem = inventory.getItem(item)
                            if not playerItem or playerItem.quantity < quantity then
                                NotifyError(player, "Not enough material: "..item.." x"..quantity)
                                validRecipe = false
                            end
                        end
                        if validRecipe then
                            for item,quantity in pairs(recipe) do
                                quantity = quantity * craftQuantity
                                if not inventory.removeItem(item, quantity) then
                                    NotifyError(player, "Crafting: Internal error", 10000)
                                    return
                                end
                            end
                            inventory.addItem(recipeItem, craftQuantity)
                        end
                    end)
                end
            end
        end
    else
        NotifyError(player, "Crafting station is too far")
    end
end
AddRemoteEvent("crafting:craft", Craft)

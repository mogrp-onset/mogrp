local Dialog = ImportPackage("dialogui")

local craftingDialog = Dialog.create("Craft", "Choose your craft", "Craft", "Cancel")
local selectInput = Dialog.addSelect(craftingDialog, 1, "Recipe:", 1, "LOADING...")
Dialog.addTextInput(craftingDialog, 1, "Quantity")

AddEvent("OnDialogSubmit", function(dialog, button, recipe, quantity)
    if dialog ~= craftingDialog then
        return
    end
    if button == 1 then
        AddPlayerChat("Craft request: "..recipe..", "..quantity)
        CallRemoteEvent("crafting:craft", recipe, quantity)
    end
end)

AddRemoteEvent("crafting:open", function(craftingStation)
    local recipes = {}
    for recipe,_ in CRAFTING_STATIONS[craftingStation].crafts do
        table.insert(recipes, recipe)
    end
    Dialog.setSelectOptions(craftingDialog, 1, selectInput, table.unpack(recipes))
    Dialog.show(craftingDialog)
end)

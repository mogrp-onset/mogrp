local _ = function(k,...) return ImportPackage("i18n").t(GetPackageName(),k,...) end

AddRemoteEvent("RotatePlayer", function(player, heading)
	SetPlayerHeading(player, heading)
end)

AddRemoteEvent("charactereditor:save", function(player, hairsChoice,
												shirtsChoice, pantsChoice, shoesChoice, colorChoice, skinChoice)
	SetPlayerProperty(player, "clothing", {
		skin_color = skinChoice,
		hair_color = colorChoice,
		hair = hairsChoice,
		shirt = shirtsChoice,
		pants = pantsChoice,
		shoes = shoesChoice
	}, function()
		SetPlayerDimension(player, 0)

		for k, v in pairs(GetStreamedPlayersForPlayer(player)) do
			if IsPlayerStreamedIn(player, v) then
				ChangeOtherPlayerClothes(v, player)
			end
		end

		UpdateClothes(player)
		SetPlayerLocation(player, PLAYER_SPAWN_POINT.x, PLAYER_SPAWN_POINT.y, PLAYER_SPAWN_POINT.z, PLAYER_SPAWN_POINT.h)
		--	SetPlayerName(player,PlayerData[player].accountid)
	end)
end)


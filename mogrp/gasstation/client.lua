local SHOW_CATEGORIES = {}
SHOW_CATEGORIES= {}
for i,category in pairs(INVENTORY_CONFIG.categories) do
    SHOW_CATEGORIES[i] = category.id
end

local gasStationUI
local vehicleId
inventoryIsOpen = false

function OnPackageStartInventory()
    gasStationUI = CreateWebUI(0, 0, 0, 0, 10, 60)
    LoadWebFile(gasStationUI, "http://asset/"..GetPackageName().."/burdigalax/client/gui/gasStation.html")
    SetWebAlignment(gasStationUI, 0.0, 0.0)
    SetWebAnchors(gasStationUI, 0.0, 0.0, 1.0, 1.0)
    SetWebVisibility(gasStationUI, WEB_HIDDEN)
end
AddEvent("OnPackageStart", OnPackageStartInventory)

function OnPackageStopInventory()
    DestroyWebUI(gasStationUI)
end
AddEvent("OnPackageStop", OnPackageStopInventory)

AddRemoteEvent("gasstation:open", function(money, _vehicleId, carTank, carFuelType, carFuel)
    AddPlayerChat("GasStationOpen("..money..", ".._vehicleId..", "..carTank..", "..carFuelType..", "..carFuel..")")

    vehicleId = _vehicleId

    GAS_STATION_CONFIG.player = {
        money = money,
        car = {
            tank = carTank,
            type = carFuelType,
            fuelValue = carFuel
        }
    }

    ExecuteWebJS(gasStationUI, "BURDIGALAX_gasStation.setConfig("..json_encode(GAS_STATION_CONFIG)..");")
    ShowMouseCursor(true)
    SetInputMode(INPUT_GAMEANDUI)
    SetWebVisibility(gasStationUI, WEB_VISIBLE)
end)

AddRemoteEvent("gasstation:error", function(title, message)
    ExecuteWebJS(gasStationUI, "BURDIGALAX_gasStation.setPaymentError("..json_encode({
        title = title,
        message = message
    })..");")
end)

AddRemoteEvent("gasstation:update_player", function(money, _vehicleId, carTank, carType, carFuel)
    vehicleId = _vehicleId
    ExecuteWebJS(gasStationUI, "BURDIGALAX_gasStation.updatePlayer("..json_encode({
        money = {
            money = money,
            car = {
                tank = carTank,
                type = carType,
                fuelValue = carFuel
            }
        }
    })..");")
end)

AddRemoteEvent("gasstation:update_gas", function(gasId, quantity, price, tax)
    ExecuteWebJS(gasStationUI, "BURDIGALAX_gasStation.updatePlayer("..json_encode({
        {
            id = gasId,
            quantity = quantity,
            price = price,
            tax = tax
        }
    })..");")
end)

AddEvent("BURDIGALAX_gasStation_onClose", function()
    ShowMouseCursor(false)
    SetInputMode(INPUT_GAME)
    Delay(100, function()
        SetWebVisibility(gasStationUI, WEB_HIDDEN)
    end)
    CallRemoteEvent("gasstation:close")
end)

AddEvent("BURDIGALAX_gasStation_onPayment", function(event)
    local data = json_decode(event)
    CallRemoteEvent("gasstation:pay", vehicleId, data.gasId, data.countBuy)
end)

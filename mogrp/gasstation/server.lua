local MogInteractiveObjects = ImportPackage("moginteractiveobjects")
local gasStations = {}

local VEHICLE_DISTANCE_CHECK = 400

-- Custom world loader
AddEvent("WorldObjectLoaded", function(objectId, data)
    if data['modelID'] ~= nil and data['specialObjectData'] and data['specialObjectData']['type'] == "gasStation" then
        if MogInteractiveObjects.RegisterInteractiveObject(5, objectId) then
            rawset(gasStations, objectId, true)
            print("GasStationRegistered: "..objectId.." !")
        else
            print("GasStationNotRegistered: "..objectId)
        end
    end
end)

local function CheckVehicleDistance(player, vehicle)
    local x, y, z = GetPlayerLocation(player)

    local x2, y2, z2 = GetVehicleLocation(vehicle)
    return GetDistance3D(x, y, z, x2, y2, z2) < VEHICLE_DISTANCE_CHECK
end

AddEvent("InteractiveObjectInteract", function(player, objectType, objectId)
    if objectType == 5 and gasStations[objectId] then
        -- FIXME move the car selection logic to client
        local nearestCar = GetNearestCar(player)
        if nearestCar ~= 0 then
            local vehicleData, vehicleConfig = GetVehicleData(nearestCar)
            if vehicleData then
                if not vehicleConfig then
                    vehicleConfig = {
                        fuelType = "fuel",
                        maxFuel = 100
                    }
                end
                -- TODO player money
                CallRemoteEvent(player, "gasstation:open", 5000, nearestCar, vehicleConfig.maxFuel,
                        vehicleConfig.fuelType, math.floor(tonumber(vehicleData.fuel)))
            else
                NotifyError(player, "No vehicle data")
            end
        else
            NotifyError(player, "No vehicle near")
        end
    end
end)

AddRemoteEvent("gasstation:pay", function(player, vehicleId, gasId, countBuy)
    if IsValidVehicle(vehicleId) then
        if CheckVehicleDistance(player, vehicleId) then
            local vehicleData, vehicleConfig = GetVehicleData(vehicleId)
            if vehicleData then
                AddPlayerChat(player, "GasStationPayed !")
                local fuel = math.floor(tonumber(vehicleData.fuel))
                if countBuy + fuel <= vehicleConfig.maxFuel then
                    vehicleData.fuel = tostring(fuel + countBuy)
                    NotifySuccess(player, "Your vehicle has been filled")
                else
                    CallRemoteEvent(player, "gasstation:error", "Error", "Your vehicle is already full")
                end
            else
                CallRemoteEvent(player, "gasstation:error", "Error", "No vehicle data")
            end
        else
            CallRemoteEvent(player, "gasstation:error", "Error", "Vehicle too far")
        end
    end
end)

function GetNearestCar(player, distance)
    if not distance then
        distance = VEHICLE_DISTANCE_CHECK
    end

    local x, y, z = GetPlayerLocation(player)

    for _,v in pairs(GetStreamedVehiclesForPlayer(player)) do
        local x2, y2, z2 = GetVehicleLocation(v)
        local dist = GetDistance3D(x, y, z, x2, y2, z2)

        if dist < distance then
            return v
        end
    end

    return 0
end

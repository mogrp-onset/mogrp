local _ = function(k,...) return ImportPackage("i18n").t(GetPackageName(),k,...) end

AddEvent("OnPackageStart", function()
    CreateTimer(function()
        for _, player in pairs(GetAllPlayers()) do
            AddPlayerHunger(player, -1)
        end
    end, 120000)

    CreateTimer(function()
        for _, player in pairs(GetAllPlayers()) do
            AddPlayerThirst(player, -1)
        end
    end, 60000)
end)

function OnPlayerDeath(player, instigator)
    SetPlayerHunger(player, 100)
    SetPlayerThirst(player, 100)
end
AddEvent("OnPlayerDeath", OnPlayerDeath)

-- Hunger
function SetPlayerHunger(player, hunger)
    if hunger > 100 then
        hunger = 100
    end

    if hunger < 0 then
        hunger = 0
    end

    SetPlayerProperty(player, "hunger", hunger, function() end)
    SetPlayerPropertyValue(player, "hunger", hunger, true)
end

function AddPlayerHunger(player, addHunger)
    GetPlayerProperty(player, "hunger", function(data)
        local hunger = 100
        if data then
            hunger = data
        end
        hunger = hunger + addHunger

        if hunger > 100 then
            hunger = 100
        end

        if hunger <= 0 then
            hunger = 0
            health = GetPlayerHealth(player) - 5
            SetPlayerHealth(player, health)
        end
        SetPlayerHunger(player, hunger)
    end)
end

-- Thirst
function SetPlayerThirst(player, thirst)
    if thirst > 100 then
        thirst = 100
    end

    if thirst < 0 then
        thirst = 0
    end

    SetPlayerProperty(player, "thirst", thirst, function() end)
    SetPlayerPropertyValue(player, "thirst", thirst, true)
end

function AddPlayerThirst(player, addThirst)
    GetPlayerProperty(player, "thirst", function(data)
        local thirst = 100
        if data then
            thirst = data
        end
        thirst = thirst + addThirst

        if thirst > 100 then
            thirst = 100
        end

        if thirst <= 0 then
            thirst = 0
            SetPlayerHealth(player, 0)
        end
        SetPlayerThirst(player, thirst)
    end)
end

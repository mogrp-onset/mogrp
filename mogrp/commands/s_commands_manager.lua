function RegisterCommand(cmd, minRights, fnc)
    print("[CommandsManager] Registering command "..cmd)
    AddCommand(cmd, function(playerId, ...)
        GetPlayer(playerId, function(player, ...)
            if not player or tonumber(player.rights) < minRights then
                AddPlayerChat(playerId, "ERROR: You are not authorized to execute this command.")
            else
                fnc(playerId, ...)
            end
        end, ...)
    end)
end
AddFunctionExport("RegisterCommand", RegisterCommand)

function OnPlayerChatCommand(player, cmd, exists)
    if not exists then
        AddPlayerChat(player, "Command '/"..cmd.."' not found!")
    end
    return true
end
AddEvent("OnPlayerChatCommand", OnPlayerChatCommand)

AddCommand("pos", function(player)
    local x, y, z = GetPlayerLocation(player)
    local h = GetPlayerHeading(player)
    AddPlayerChat(player, "Position: "..x..", "..y..", "..z..", "..h)
    return
end)

AddCommand("kill", function(player)
    SetPlayerHealth(player, 0)
end)

local MogZones = ImportPackage("mogzones")
AddCommand("test", function(player, size)
    if not size then
        size = 100
    end
    local x, y, z = GetPlayerLocation(player)
    MogZones.NewCircleZone(x, y, size)
end)

AddEvent("mogzones:zone_enter", function(player, zoneId)
    AddPlayerChat(player, "Enter "..zoneId)
end)

AddEvent("mogzones:zone_exit", function(player, zoneId)
    AddPlayerChat(player, "Exit "..zoneId)
end)


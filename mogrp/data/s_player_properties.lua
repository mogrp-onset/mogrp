local handle
local playerPropertiesCache = Map:new()

AddEvent("OnPackageStart", function()
    handle = GetDbHandle("general")

    -- Clear outdated keys
    CreateTimer(function()
        local time = os.time()
        for k, v in pairs(playerPropertiesCache:items()) do
            for k1, v1 in pairs(v:items()) do
                if v1.lastAccess < time - 600 then
                    v:remove(k1)
                end
            end
            if v:length() <= 0 then
                playerPropertiesCache:remove(k)
            end
        end
    end, 60000)
end)

function GetPlayerProperty(player, property, handler, ...)
    local propertyValue
    if playerPropertiesCache:get(player) ~= nil then
        propertyValue = playerPropertiesCache:get(player):get(property)
    end
    if (propertyValue == nil) then
        mariadb_async_query(handle, mariadb_prepare(handle,
                "SELECT value FROM player_properties WHERE steam_id = '?' AND `key` = '?'",
                tostring(GetPlayerSteamId(player)),
                property
        ), function(...)
            local value
            if (mariadb_get_row_count() ~= 0) then
                local result = mariadb_get_assoc(1)
                if result['value'] then
                    value = json_decode(result['value'])
                    local playerCacheData = playerPropertiesCache:get(player)
                    if playerCacheData == nil then
                        playerCacheData = Map:new()
                        playerPropertiesCache:put(player, playerCacheData)
                    end
                    playerCacheData:put(property, {
                        lastAccess = os.time(),
                        value = value
                    })
                end
            end
            handler(value, ...)
        end, ...)
    else
        propertyValue.lastAccess = os.time()
        handler(propertyValue.value, ...)
    end
end
AddFunctionExport("GetPlayerProperty", GetPlayerProperty)

function SetPlayerProperty(player, property, value, handler, ...)
    mariadb_async_query(handle, mariadb_prepare(handle,
            "REPLACE INTO player_properties (steam_id, `key`, value) VALUES ('?', '?', '?')",
            tostring(GetPlayerSteamId(player)),
            property,
            json_encode(value)
    ), function(...)
        local playerCacheData = playerPropertiesCache:get(player)
        if playerCacheData == nil then
            playerCacheData = Map:new()
            playerPropertiesCache:put(player, playerCacheData)
        end
        playerCacheData:put(property, {
            lastAccess = os.time(),
            value = value
        })
        handler(...)
    end, ...)
end
AddFunctionExport("SetPlayerProperty", SetPlayerProperty)

function DeletePlayerProperty(player, property, handler, ...)
    mariadb_async_query(handle, mariadb_prepare(handle,
            "DELETE FROM player_properties WHERE steam_id = '?' AND `key` = '?'",
            tostring(GetPlayerSteamId(player)),
            property
    ), function(...)
        local playerCacheData = playerPropertiesCache:get(player)
        if playerCacheData ~= nil then
            if playerCacheData:get(property) ~= nil then
                playerCacheData:remove(property)
            end
            if playerCacheData:length() <= 0 then
                playerPropertiesCache:remove(player)
            end
        end
        handler(...)
    end, ...)
end
AddFunctionExport("DeletePlayerProperty", DeletePlayerProperty)

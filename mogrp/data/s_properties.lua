local handle
local cache = Map:new()

AddEvent("OnPackageStart", function()
    handle = GetDbHandle("general")

    -- Clear outdated keys
    CreateTimer(function()
        local time = os.time()
        for k, v in pairs(cache:items()) do
            if v.lastAccess < time - 600 then
                cache:remove(k)
            end
        end
    end, 60000)
end)

function GetProperty(property, handler, ...)
    local propertyValue = cache:get(property)
    if (propertyValue == nil) then
        mariadb_async_query(handle, mariadb_prepare(handle,
                "SELECT value FROM properties WHERE `key` = '?'",
                property
        ), function(...)
            local value
            if (mariadb_get_row_count() ~= 0) then
                local result = mariadb_get_assoc(1)
                if result['value'] ~= nil then
                    value = json_decode(result['value'])
                    cache:put(property, {
                        lastAccess = os.time(),
                        value = value
                    })
                end
            end
            handler(value, ...)
        end, ...)
    else
        propertyValue.lastAccess = os.time()
        handler(propertyValue.value, ...)
    end
end
AddFunctionExport("GetProperty", GetProperty)

function SetProperty(property, value, handler, ...)
    mariadb_async_query(handle, mariadb_prepare(handle,
            "REPLACE INTO properties (`key`, value) VALUES ('?', '?')",
            property,
            json_encode(value)
    ), function(...)
        cache:put(property, {
            lastAccess = os.time(),
            value = value
        })
        handler(...)
    end, ...)
end
AddFunctionExport("SetProperty", SetProperty)

function DeleteProperty(property, handler, ...)
    mariadb_async_query(handle, mariadb_prepare(handle,
            "DELETE FROM properties WHERE `key` = '?'",
            property
    ), function(...)
        cache:remove(property)
        handler(...)
    end, ...)
end
AddFunctionExport("DeleteProperty", DeleteProperty)

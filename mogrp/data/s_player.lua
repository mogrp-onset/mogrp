local handle
local cache = Map:new()

AddEvent("OnPackageStart", function()
    handle = GetDbHandle("general")

    -- Clear outdated keys
    CreateTimer(function()
        local time = os.time()
        for k, v in pairs(cache:items()) do
            if v.lastAccess < time - 600 then
                cache:remove(k)
            end
        end
    end, 60000)
end)

function GetPlayer(player, handler, ...)
    local propertyValue = cache:get(player)
    if (propertyValue == nil) then
        mariadb_async_query(handle, mariadb_prepare(handle,
                "SELECT * FROM players WHERE steam_id = '?'",
                tostring(GetPlayerSteamId(player))
        ), function(...)
            local value
            if (mariadb_get_row_count() ~= 0) then
                value = mariadb_get_assoc(1)
                cache:put(player, {
                    lastAccess = os.time(),
                    value = value
                })
            end
            handler(value, ...)
        end, ...)
    else
        propertyValue.lastAccess = os.time()
        handler(propertyValue.value, ...)
    end
end
AddFunctionExport("GetPlayer", GetPlayer)

function UpdatePlayer(player, parameters, handler, ...)
    local keyStr = ""
    local valStr = ""
    local values = {}
    values[1] = tostring(GetPlayerSteamId(player))
    local idx = 2
    for key, value in pairs(parameters) do
        keyStr = keyStr..", "..key
        valStr = valStr..", '?'"
        values[idx] = value
        idx = idx + 1
    end
    mariadb_async_query(handle, mariadb_prepare(handle,
            "REPLACE INTO players (steam_id"..keyStr..") VALUES ('?'"..valStr..")",
            table.unpack(values)
    ), function(...)
        cache:remove(player);
        handler(...)
    end, ...)
end
AddFunctionExport("UpdatePlayer", UpdatePlayer)

math.randomseed(os.time()) -- Set seed

function generateRandomString(length)
    if length == nil then length = 1 end
    local value = ""
    for _ = 1, length do
        local rand = string.char(math.random(65,90))
        value = value .."".. rand
    end
    return value
end

function generateRandomNumberString(length)
    if length == nil then length = 1 end
    local value = ""
    for _ = 1, length do
        local rand = tostring(math.random(0,9))
        value = value .. "" .. rand
    end
    return value
end

function getWeightedRandom(array)
    local sum = 0
    local entries = {}
    for _,entry in ipairs(array) do
        entry.weight = entry.weight or 1
        sum = sum + entry.weight
        table.insert(entries, entry)
    end
    table.sort(entries, function(a, b)
        return a.weight - b.weight
    end)
    local rand = math.random(0, sum)
    for _,entry in ipairs(entries) do
        if rand < entry.weight then
            return entry
        end
        rand = rand - entry.weight
    end
    return nil
end

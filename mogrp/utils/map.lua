Map = {}
Map.__index = Map

function Map:new()
    local map = {}
    setmetatable(map, Map)
    map._length = 0
    map._items = {}
    return map
end

function Map:length()
    return self._length
end

function Map:get(key)
    return self._items[key]
end

function Map:put(key, value)
    if value == nil then
        print("Map value cannot be nil.")
        return nil
    end
    local updated = true
    if self._items[key] == nil then
        updated = false
        self._length = self._length + 1
    end
    self._items[key] = value
    return updated
end

function Map:remove(key)
    local value
    if self._items[key] ~= nil then
        value = self._items[key]
        self._items[key] = nil
        self._length = self._length - 1
    end
    return value
end

function Map:items()
    return self._items
end

Cache = {}
Cache.__index = Cache

local caches = {}

-- This function checks all caches
AddEvent("OnPackageStart", function()
    -- Clear outdated keys
    CreateTimer(function()
        for k, v in pairs(caches) do
            v:clearExpiredData()
        end
    end, 1000)
end)

--- Creates a new cache
--- @param timeout number timeout of the cached data in seconds
function Cache:new(timeout)
    local cache = {}
    setmetatable(cache, Cache)
    cache._map = Map:new()
    cache._timeout = timeout
    caches[cache] = cache
    return cache
end

function Cache:get(key)
    local item = self._map:get(key)
    if item then
        item.lastAccess = os.time()
    else
        return nil
    end
    return item.value
end

function Cache:put(key, value)
    if value == nil then
        print("Cache value cannot be nil.")
        return false
    end
    self._map:put(key, {
        lastAccess = os.time(),
        value = value
    })
    return true
end

function Cache:remove(key)
    return self._map:remove(key)
end

function Cache:clearExpiredData()
    local time = os.time()
    for k,v in pairs(self._map:items()) do
        if v.lastAccess < time - self._timeout then
            self:remove(k)
        end
    end
end
